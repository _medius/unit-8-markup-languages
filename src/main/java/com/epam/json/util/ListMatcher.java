package com.epam.json.util;

import java.util.List;

public class ListMatcher {

	public static boolean isEqualLists(List<?> oneList, List<?> twoList) {
		if (oneList.size() != twoList.size()) {
			return false;
		}

		for (int i = 0; i < oneList.size(); i++) {
			if (!oneList.get(i).equals(twoList.get(i))) {
				return false;
			}
		}

		return true;
	}
}

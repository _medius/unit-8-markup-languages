package com.epam.json;

import com.epam.json.adapters.*;
import com.epam.json.pojos.Example;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.json.*;
import javax.json.stream.JsonParser;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.OffsetTime;
import java.util.List;

public class MainForJson {

	private static final String NODE_NAME_TO_FIND = "Brand";

	private File filePath = new File(System.getProperty("user.dir") + "\\example.json");
	private File newFilePath = new File(System.getProperty("user.dir") + "\\newExample.json");

	private Gson gson = new GsonBuilder()
			.setPrettyPrinting()
			.registerTypeAdapter(OffsetTime.class, new TimeDeserializer())
			.registerTypeAdapter(OffsetTime.class, new TimeSerializer())
			.registerTypeAdapter(Integer.class, new IntegerSerializer())
			.registerTypeAdapter(Double.class, new DoubleSerializer())
			.create();

	private void firstTask() {
		try (JsonParser parser = Json.createParser(new FileInputStream(filePath))) {
			JsonParser.Event event;

			String name = null;
			String type;
			String value;

			boolean nodeFound = false;
			while (!nodeFound) {
				event = parser.next();
				if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(NODE_NAME_TO_FIND)) {
					name = parser.getString();
					nodeFound = true;
				}
			}

			parser.next();

			JsonValue jsonValue = parser.getValue();
			type = jsonValue.getValueType().toString();
			value = jsonValue.toString();

			System.out.println("First task result:");
			System.out.printf("Node with name %s has type %s and value: %s\n", name, type, value);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void thirdTask() {
		Example example = readObjectFromJson(filePath, Example.class);

		writeObjectToJson(newFilePath, example);

		Example newExample = readObjectFromJson(newFilePath, Example.class);

		System.out.println("\nThird task result:");
		compareExamples(example, newExample);
		compareJsons(filePath, newFilePath);
	}

	private <T> void writeObjectToJson(File path, T object) {
		try (FileWriter fileWriter = new FileWriter(path)) {
			gson.toJson(object, fileWriter);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private <T> T readObjectFromJson(File path, Class<T> aClass) {
		T object = null;
		try (FileReader fileReader = new FileReader(path)) {
			object = gson.fromJson(fileReader, aClass);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return object;
	}

	private void compareExamples(Example oneExample, Example twoExample) {
		if (!oneExample.equals(twoExample)) {
			System.out.println("Examples are not equals.");
		} else {
			System.out.println("Examples are equals.");
		}
	}

	private void compareJsons(File pathToFirst, File pathToSecond) {
		try {
			List<String> firstList = Files
					.readAllLines(Paths.get(pathToFirst.getAbsolutePath()), StandardCharsets.UTF_8);

			List<String> secondList = Files
					.readAllLines(Paths.get(pathToSecond.getAbsolutePath()), StandardCharsets.UTF_8);

			for (int i = 0; i < firstList.size(); i++) {
				String string = firstList.get(i).trim().replaceAll(" ", "");
				String newString = secondList.get(i).trim().replaceAll(" ", "");
				if (!string.equals(newString)) {
					System.out.println("JSONs are not equals.");
					return ;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("JSONs are equals.");
	}

	private void appStart() {
		firstTask();
		thirdTask();
	}

	public static void main(String[] args) {
		new MainForJson().appStart();
	}
}

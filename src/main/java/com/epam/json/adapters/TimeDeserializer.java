package com.epam.json.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;

public class TimeDeserializer implements JsonDeserializer<OffsetTime> {

	@Override
	public OffsetTime deserialize(JsonElement element, Type type, JsonDeserializationContext jsonDeserializationContext)
			throws JsonParseException {
		String time = element.getAsString();

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ssZ");
		return OffsetTime.parse(time, dateTimeFormatter);
	}
}

package com.epam.json.adapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class IntegerSerializer implements JsonSerializer<Integer> {
	@Override
	public JsonElement serialize(Integer integer, Type type, JsonSerializationContext jsonSerializationContext) {
		String value;
		if (integer == 0) {
			value = "00";
		} else {
			value = integer.toString();
		}
		String stringToParse = "\"" + value + "\"";
		JsonParser parser = new JsonParser();
		return parser.parse(stringToParse);
	}
}

package com.epam.json.adapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class DoubleSerializer implements JsonSerializer<Double> {

	@Override
	public JsonElement serialize(Double aDouble, Type type, JsonSerializationContext jsonSerializationContext) {
		String stringToParse = "\"" + aDouble.toString() + "\"";
		JsonParser parser = new JsonParser();
		return parser.parse(stringToParse);
	}
}

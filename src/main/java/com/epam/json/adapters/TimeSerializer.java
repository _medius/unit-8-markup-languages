package com.epam.json.adapters;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;

public class TimeSerializer implements JsonSerializer<OffsetTime> {

	@Override
	public JsonElement serialize(OffsetTime offsetTime, Type type, JsonSerializationContext jsonSerializationContext) {
		String time = "\"" + offsetTime.format(DateTimeFormatter.ofPattern("HH:mm:ssZ")) + "\"";

		JsonParser parser = new JsonParser();
		return parser.parse(time);
	}
}

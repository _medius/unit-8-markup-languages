package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostalAddress implements Serializable {

	@SerializedName("AddressLine")
	@Expose
	private List<String> addressLine = new ArrayList<>();

	@SerializedName("TownName")
	@Expose
	private String townName;

	@SerializedName("CountrySubDivision")
	@Expose
	private List<String> countrySubDivision = new ArrayList<>();

	@SerializedName("Country")
	@Expose
	private String country;

	@SerializedName("PostCode")
	@Expose
	private String postCode;

	@SerializedName("GeoLocation")
	@Expose
	private GeoLocation geoLocation;

	private final static long serialVersionUID = -7269901370152761352L;

	public List<String> getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(List<String> addressLine) {
		this.addressLine = addressLine;
	}

	public String getTownName() {
		return townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	public List<String> getCountrySubDivision() {
		return countrySubDivision;
	}

	public void setCountrySubDivision(List<String> countrySubDivision) {
		this.countrySubDivision = countrySubDivision;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public GeoLocation getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		PostalAddress newObj = (PostalAddress) obj;

		if (!this.townName.equals(newObj.getTownName())) {
			System.out.println("In PostalAddress townNames are not equals.");
			return false;
		}

		if (!this.country.equals(newObj.getCountry())) {
			System.out.println("In PostalAddress countries are not equals.");
			return false;
		}

		if (!this.postCode.equals(newObj.getPostCode())) {
			System.out.println("In PostalAddress postCodes are not equals.");
			return false;
		}

		if (!this.geoLocation.equals(newObj.getGeoLocation())) {
			System.out.println("In PostalAddress geoLocations are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.addressLine, newObj.getAddressLine())) {
			System.out.println("In PostalAddress addressLines are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.countrySubDivision, newObj.getCountrySubDivision())) {
			System.out.println("In PostalAddress countrySubDivisions are not equals.");
			return false;
		}

		return true;
	}
}
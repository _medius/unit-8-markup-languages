package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable {

	@SerializedName("Brand")
	@Expose
	private List<Brand> brand = new ArrayList<>();

	private final static long serialVersionUID = 6946168872851196555L;

	public List<Brand> getBrand() {
		return brand;
	}

	public void setBrand(List<Brand> brand) {
		this.brand = brand;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		Data newObj = (Data) obj;

		if (!ListMatcher.isEqualLists(this.brand, newObj.getBrand())) {
			System.out.println("In Data brands are not equals.");
			return false;
		}

		return true;
	}
}
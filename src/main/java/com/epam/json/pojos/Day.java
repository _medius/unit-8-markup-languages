package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Day implements Serializable {

	@SerializedName("Name")
	@Expose
	private String name;

	@SerializedName("OpeningHours")
	@Expose
	private List<OpeningHour> openingHours = new ArrayList<>();

	private final static long serialVersionUID = -5273236561150412244L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<OpeningHour> getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(List<OpeningHour> openingHours) {
		this.openingHours = openingHours;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		Day newObj = (Day) obj;

		if (!this.name.equals(newObj.getName())) {
			System.out.println("In Day names are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.openingHours, newObj.getOpeningHours())) {
			System.out.println("In Day openingHours are not equals.");
			return false;
		}

		return true;
	}
}
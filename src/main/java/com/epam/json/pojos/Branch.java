package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Branch implements Serializable {

	@SerializedName("Identification")
	@Expose
	private int identification;

	@SerializedName("SequenceNumber")
	@Expose
	private int sequenceNumber;

	@SerializedName("Name")
	@Expose
	private String name;

	@SerializedName("Type")
	@Expose
	private String type;

	@SerializedName("CustomerSegment")
	@Expose
	private List<String> customerSegment = new ArrayList<>();

	@SerializedName("Accessibility")
	@Expose
	private List<String> accessibility = new ArrayList<>();

	@SerializedName("OtherServiceAndFacility")
	@Expose
	private List<OtherServiceAndFacility> otherServiceAndFacility = new ArrayList<>();

	@SerializedName("Availability")
	@Expose
	private Availability availability;

	@SerializedName("ContactInfo")
	@Expose
	private List<ContactInfo> contactInfo = new ArrayList<>();

	@SerializedName("PostalAddress")
	@Expose
	private PostalAddress postalAddress;

	private final static long serialVersionUID = -9170910589861498351L;

	public int getIdentification() {
		return identification;
	}

	public void setIdentification(int identification) {
		this.identification = identification;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getCustomerSegment() {
		return customerSegment;
	}

	public void setCustomerSegment(List<String> customerSegment) {
		this.customerSegment = customerSegment;
	}

	public List<String> getAccessibility() {
		return accessibility;
	}

	public void setAccessibility(List<String> accessibility) {
		this.accessibility = accessibility;
	}

	public List<OtherServiceAndFacility> getOtherServiceAndFacility() {
		return otherServiceAndFacility;
	}

	public void setOtherServiceAndFacility(List<OtherServiceAndFacility> otherServiceAndFacility) {
		this.otherServiceAndFacility = otherServiceAndFacility;
	}

	public Availability getAvailability() {
		return availability;
	}

	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	public List<ContactInfo> getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(List<ContactInfo> contactInfo) {
		this.contactInfo = contactInfo;
	}

	public PostalAddress getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(PostalAddress postalAddress) {
		this.postalAddress = postalAddress;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		Branch newObj = (Branch) obj;

		if (!(this.identification == newObj.getIdentification())) {
			System.out.println("In Branch identifications are not equals.");
			return false;
		}

		if (!(this.sequenceNumber == newObj.getSequenceNumber())) {
			System.out.println("In Branch sequenceNumbers are not equals.");
			return false;
		}

		if (!this.name.equals(newObj.getName())) {
			System.out.println("In Branch names are not equals.");
			return false;
		}

		if (!this.type.equals(newObj.getType())) {
			System.out.println("In Branch types are not equals.");
			return false;
		}

		if (!this.availability.equals(newObj.getAvailability())) {
			System.out.println("In Branch availabilities are not equals.");
			return false;
		}

		if (!this.postalAddress.equals(newObj.getPostalAddress())) {
			System.out.println("In Branch postalAdresses are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.accessibility, newObj.getAccessibility())) {
			System.out.println("In Branch accessibility are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.customerSegment, newObj.getCustomerSegment())) {
			System.out.println("In Branch customerSegments are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.otherServiceAndFacility, newObj.getOtherServiceAndFacility())) {
			System.out.println("In Branch otherServiceAndFacilities are not equals.");
			return false;
		}

		if (!ListMatcher.isEqualLists(this.contactInfo, newObj.getContactInfo())) {
			System.out.println("In Branch otherServiceAndFacilities are not equals.");
			return false;
		}

		return true;
	}

}
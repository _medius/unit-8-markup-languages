package com.epam.json.pojos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoLocation implements Serializable {

	@SerializedName("GeographicCoordinates")
	@Expose
	private GeographicCoordinates geographicCoordinates;

	private final static long serialVersionUID = 5623645287256206977L;

	public GeographicCoordinates getGeographicCoordinates() {
		return geographicCoordinates;
	}

	public void setGeographicCoordinates(GeographicCoordinates geographicCoordinates) {
		this.geographicCoordinates = geographicCoordinates;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		GeoLocation newObj = (GeoLocation) obj;

		if (!this.geographicCoordinates.equals(newObj.getGeographicCoordinates())) {
			System.out.println("In GeoLocation geographicCoordinates are not equals.");
			return false;
		}

		return true;
	}
}
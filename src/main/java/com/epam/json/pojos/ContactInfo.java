package com.epam.json.pojos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactInfo implements Serializable {

	@SerializedName("ContactType")
	@Expose
	private String contactType;

	@SerializedName("ContactContent")
	@Expose
	private String contactContent;

	private final static long serialVersionUID = -7666636881645831999L;

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getContactContent() {
		return contactContent;
	}

	public void setContactContent(String contactContent) {
		this.contactContent = contactContent;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		ContactInfo newObj = (ContactInfo) obj;

		if (!this.contactContent.equals(newObj.getContactContent())) {
			System.out.println("In ContactType contactContents are not equals.");
			return false;
		}

		if (!this.contactType.equals(newObj.getContactType())) {
			System.out.println("In ContactType contactTypes are not equals.");
			return false;
		}

		return true;
	}
}
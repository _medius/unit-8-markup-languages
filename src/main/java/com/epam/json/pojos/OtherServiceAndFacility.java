package com.epam.json.pojos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtherServiceAndFacility implements Serializable {

	@SerializedName("Code")
	@Expose
	private String code;

	@SerializedName("Name")
	@Expose
	private String name;

	private final static long serialVersionUID = -5257760737403540668L;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		OtherServiceAndFacility newObj = (OtherServiceAndFacility) obj;

		if (!this.name.equals(newObj.getName())) {
			System.out.println("In OtherServiceAndFacilities names are not equals.");
			return false;
		}

		if (!this.code.equals(newObj.getCode())) {
			System.out.println("In OtherServiceAndFacilities codes are not equals.");
			return false;
		}

		return true;
	}
}
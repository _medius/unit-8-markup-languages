package com.epam.json.pojos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Availability implements Serializable {

	@SerializedName("StandardAvailability")
	@Expose
	private StandardAvailability standardAvailability;

	private final static long serialVersionUID = -7844015202527415757L;

	public StandardAvailability getStandardAvailability() {
		return standardAvailability;
	}

	public void setStandardAvailability(StandardAvailability standardAvailability) {
		this.standardAvailability = standardAvailability;
	}

	@Override
	public boolean equals(Object obj) {

		 if (this == obj) {
		 	return true;
		 }

		 if (obj == null || this.getClass() != obj.getClass()) {
		 	return false;
		 }

		Availability newObj = (Availability) obj;

		if (!this.standardAvailability.equals(newObj.getStandardAvailability())) {
			System.out.println("In Availability standardAvailabilities are not equals.");
			return false;
		}

		return true;
	}
}
package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandardAvailability implements Serializable {

	@SerializedName("Day")
	@Expose
	private List<Day> day = new ArrayList<>();

	private final static long serialVersionUID = -3683820519988357836L;

	public List<Day> getDay() {
		return day;
	}

	public void setDay(List<Day> day) {
		this.day = day;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		StandardAvailability newObj = (StandardAvailability) obj;

		if (!ListMatcher.isEqualLists(this.day, newObj.getDay())) {
			System.out.println("In Data brands are not equals.");
			return false;
		}

		return true;
	}
}
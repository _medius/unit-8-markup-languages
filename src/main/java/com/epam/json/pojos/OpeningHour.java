package com.epam.json.pojos;

import java.io.Serializable;
import java.time.OffsetTime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpeningHour implements Serializable {

	@SerializedName("OpeningTime")
	@Expose
	private OffsetTime openingTime;

	@SerializedName("ClosingTime")
	@Expose
	private OffsetTime closingTime;

	private final static long serialVersionUID = -2975534814103232808L;

	public OffsetTime getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(OffsetTime openingTime) {
		this.openingTime = openingTime;
	}

	public OffsetTime getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(OffsetTime closingTime) {
		this.closingTime = closingTime;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		OpeningHour newObj = (OpeningHour) obj;

		if (!this.openingTime.equals(newObj.getOpeningTime())) {
			System.out.println("In OpeningHour openingTimes are not equals.");
			return false;
		}

		if (!this.closingTime.equals(newObj.getClosingTime())) {
			System.out.println("In OpeningHour closingTimes are not equals.");
			return false;
		}

		return true;
	}
}
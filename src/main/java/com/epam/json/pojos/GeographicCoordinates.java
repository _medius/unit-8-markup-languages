package com.epam.json.pojos;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeographicCoordinates implements Serializable {

	@SerializedName("Latitude")
	@Expose
	private double latitude;

	@SerializedName("Longitude")
	@Expose
	private double longitude;

	private final static long serialVersionUID = -7929939534417662505L;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		GeographicCoordinates newObj = (GeographicCoordinates) obj;

		if (this.latitude != newObj.getLatitude()) {
			System.out.println("In GeographicCoordinates latitudes are not equals.");
			return false;
		}

		if (this.longitude != newObj.longitude) {
			System.out.println("In GeographicCoordinates longitudes are not equals.");
			return false;
		}

		return true;
	}
}
package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example implements Serializable {

	@SerializedName("data")
	@Expose
	private List<Data> data = new ArrayList<>();

	private final static long serialVersionUID = 8859009251529085572L;

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		Example newExample = (Example) obj;

		if (!ListMatcher.isEqualLists(this.data, newExample.getData())) {
			System.out.println("In Example data are not equals.");
			return false;
		}

		return true;
	}
}
package com.epam.json.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.epam.json.util.ListMatcher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Brand implements Serializable {

	@SerializedName("BrandName")
	@Expose
	private String brandName;

	@SerializedName("Branch")
	@Expose
	private List<Branch> branch = new ArrayList<>();

	private final static long serialVersionUID = 4569103083217122500L;

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public List<Branch> getBranch() {
		return branch;
	}

	public void setBranch(List<Branch> branch) {
		this.branch = branch;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}

		Brand newObj = (Brand) obj;

		if (!ListMatcher.isEqualLists(this.branch, newObj.getBranch())) {
			System.out.println("In Brand Branches are not equals.");
			return false;
		}

		if (!this.brandName.equals(newObj.getBrandName())) {
			System.out.println("In Brand brandNames are not equals.");
			return false;
		}

		return true;
	}
}
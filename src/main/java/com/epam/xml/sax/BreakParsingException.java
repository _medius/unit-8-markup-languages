package com.epam.xml.sax;

import org.xml.sax.SAXException;

public class BreakParsingException extends SAXException {

	public BreakParsingException() {
	}

	public BreakParsingException(String message) {
		super(message);
	}
}

package com.epam.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class CustomHandler extends DefaultHandler {

	private static final String ATTRIBUTE_NAME_TO_FIND = "OBS_STATUS";
	private static final String ATTRIBUTE_VALUE_TO_FIND = "M";

	@Override
	public void startDocument() throws SAXException {
		System.out.println("Start parsing document by SAX.");
	}

	@Override
	public void endDocument() throws SAXException {
		throw new SAXException("Element not found.");
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		for (int i = 0; i < attributes.getLength(); i++) {
			if (attributes.getLocalName(i).equals(ATTRIBUTE_NAME_TO_FIND)) {
				if (attributes.getValue(i).equals(ATTRIBUTE_VALUE_TO_FIND)) {
					throw new BreakParsingException("Element with attribute OBS_STATUS = M is found.");
				}
			}
		}
	}
}

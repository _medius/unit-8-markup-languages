package com.epam.xml;

import com.epam.xml.sax.BreakParsingException;
import com.epam.xml.sax.CustomHandler;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;

public class MainForXML {

	private File path = new File(System.getProperty("user.dir") + "\\retrieveCurrentCPFFUNCP.xml");

	private void fourthTask() {
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		SAXParser parser = null;

		try {
			parser = parserFactory.newSAXParser();
		} catch (SAXException | ParserConfigurationException e) {
			e.printStackTrace();
		}

		CustomHandler customHandler = new CustomHandler();

		try {
			parser.parse(path, customHandler);
		} catch (IOException | SAXException e) {
			if (e instanceof BreakParsingException) {
				System.out.println(e.getMessage());
			} else {
				e.printStackTrace();
			}
		}
	}

	private void fifthTask() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document document = null;
		try {
			document = builder.parse(path);
		} catch (IOException |SAXException e) {
			e.printStackTrace();
		}

		if (document != null) {
			System.out.println("Document has been created by DOM parser.");
		}
	}

	private void appStart() {
		fourthTask();
		fifthTask();
	}

	public static void main(String[] args) {
		new MainForXML().appStart();
	}
}

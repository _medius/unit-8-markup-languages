# Markup Languages Homework

1. First three tasks is in the src.main.java.com.epam.json package. 
I used GSON lib with custom serializers and deserializers to correct 
parse some data types (OffsetTime, Double, Integer). Classes of objects
to deserialize example.json is in the json.pojos directory. All time,
int and double data types parses correctly.

2. Last two tasks is in the src.main.java.com.epam.xml package.
In the xml.sax folder you can find Handler to SAX and custom exception 
to stop parser when it find element.